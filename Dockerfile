FROM mariadb:10.6-focal

RUN apt-get update && apt-get dist-upgrade -y
RUN apt-get install -y wget
RUN wget -nv https://www.fuzzwork.co.uk/dump/mysql-latest.tar.bz2
RUN tar -xf mysql-latest.tar.bz2
RUN mv sde-*/*.sql /docker-entrypoint-initdb.d/sde_eve_online.sql

