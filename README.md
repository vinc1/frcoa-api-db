# FRCOA-API-DB

## Description


Ce projet à pour but de fournir une image docker mariadb avec la base de donnée convertie depuis la SDE (Static Data Export) de ccp par https://www.fuzzwork.co.uk/.
Cette image permet un deploiment facilité de la base de donnée de l'api.


- la base de donnée peut etre trouvée à cette adresse : https://www.fuzzwork.co.uk/dump/
- source officielle : https://developers.eveonline.com/resource -> lien "SDE Conversions" en bas de page
- le format natif de la SDE fournie par CCP est au format YAML. https://eve-static-data-export.s3-eu-west-1.amazonaws.com/tranquility/sde.zip

## Utilisation


- les variables d'environnement
  - MYSQL_ROOT_PASSWORD *obligatoire pour le fonctionnement de l'image mariadb https://hub.docker.com/_/mariadb*
  - MYSQL_DATABASE 
    - *obligatoire le conteneur docker va installer automatiquement la base de fuzzwork dans cette base*
    - *doit correspondre au parametrage de l'api*
- Exemples d'utilisation : 
  - si vous souhaitez build l'image (télécharge automatiquement la dernière version du SDE converti chez fuzzwork)
    - `docker build -t registry.gitlab.com/vinc1/frcoa-api-db:latest .`
  - sinon vous pouvez pull l'image depuis le repository du projet
    - `docker pull registry.gitlab.com/vinc1/frcoa-api-db:latest`
  - puis lancez le container mariadb __(pensez a changer les mot de passe !)__
    - `docker run -p 3306 --env MYSQL_ROOT_PASSWORD=frcoa-api-db --env MYSQL_DATABASE=sde_eve_online --name eve-db --restart unless-stopped --detach registry.gitlab.com/vinc1/frcoa-api-db:latest`

### Participation 

si vous souhaitez participer a ce projet d'api merci de me contacter par discord vinc1#4890 afin d'obtenir l'accès developpeur.
